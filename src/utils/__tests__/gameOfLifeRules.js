import { getNumberLivingNeighbors } from '../gameOfLifeRules';
import computeCellTargetState from '../gameOfLifeRules';

describe('Testing Game of Life logic', () => {

    it('should compute the number of neighbors accurately', () => {
        // Testing using a full 3x3 grid
        const grid = [
            [true, true, true],
            [true, true, true],
            [true, true, true]];

        const expectedNeighbors = [
            [3, 5, 3],
            [5, 8, 5],
            [3, 5, 3]
        ];

        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                let numberOfNeighbors = getNumberLivingNeighbors(grid, i, j);
                expect(numberOfNeighbors).toBe(expectedNeighbors[i][j]);
            }
        }
    });

    it('should compute Zero neighbors when the grid is empty', () => {
        // Testing using a full 3x3 grid
        const grid = [
            [false, false, false],
            [false, false, false],
            [false, false, false]];

        for (let i = 0; i < 3; i++) {
            for (let j = 0; j < 3; j++) {
                let numberOfNeighbors = getNumberLivingNeighbors(grid, i, j);
                expect(numberOfNeighbors).toBe(0);
            }
        }
    });

    it('should apply rule 1: Any live cell with two live neighbours survives', () => {
        // Testing the rules on the first cell
        const grid = [
            [true, false],
            [true, true]];

        const newCellState = computeCellTargetState(grid, 0, 0);
        expect(newCellState).toBe(true);
    });


    it('should apply rule 1: Any live cell with three live neighbours survives', () => {
        // Testing the rules on the first cell
        const grid = [
            [true, true],
            [true, true]];

        const newCellState = computeCellTargetState(grid, 0, 0);
        expect(newCellState).toBe(true);
    });

    it('should apply rule 2: Any dead cell with three live neighbours becomes a live cell', () => {
        // Testing the rules on the first cell
        const grid = [
            [false, true],
            [true, true]];

        const newCellState = computeCellTargetState(grid, 0, 0);
        expect(newCellState).toBe(true);
    });

    it('should apply rule 3: All other live cells die in the next generation. Similarly, all other dead cells stay dead (1 neighbor)', () => {
        // Testing the rules on the first cell
        const grid = [
            [true, false],
            [true, false]];

        const newCellState = computeCellTargetState(grid, 0, 0);
        expect(newCellState).toBe(false);
    });

    it('should apply rule 3: All other live cells die in the next generation. Similarly, all other dead cells stay dead (0 neighbor)', () => {
        // Testing the rules on the first cell
        const grid = [
            [true, false],
            [false, false]];

        const newCellState = computeCellTargetState(grid, 0, 0);
        expect(newCellState).toBe(false);
    });

});