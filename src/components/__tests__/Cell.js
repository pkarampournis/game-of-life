import React from 'react';
import Cell from '../Cell';
import { shallow } from 'enzyme';

describe('Cell component tests', () => {
    const defaultProps = {
        row: 1,
        column: 1,
        toggleCellState: jest.fn((row, col) => ({})),
        cellState: true
    };

    it('Should Render properly', () => {
        const wrapper = shallow(<Cell {...defaultProps} />);
        expect(wrapper.hasClass('cell')).toBe(true);
    });
});