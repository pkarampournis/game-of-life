import React from 'react';
import Grid from '../Grid';
import { shallow } from 'enzyme';
import Row from '../Row';

describe('Grid component tests', () => {

    it('Should Render properly', () => {
        const wrapper = shallow(<Grid />);
        expect(wrapper.find('.gridContainer')).toHaveLength(1);
        expect(wrapper.find(Row)).toHaveLength(9);
    });
});