import React from 'react';
import { shallow } from 'enzyme';
import Slider from '../Slider';

describe('Slider component tests', () => {
    const defaultProps = {
        setTimerInterval: jest.fn()
    };

    it('Should Render properly', () => {
        const wrapper = shallow(<Slider {...defaultProps} />);
        expect(wrapper.hasClass('slidecontainer')).toBe(true);
        expect(wrapper.contains(<h4>Game speed</h4>)).toBe(true);
    });
});