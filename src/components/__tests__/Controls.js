import React from 'react';
import Controls from '../Controls';
import { shallow } from 'enzyme';

describe('Controls component tests', () => {
    const defaultProps = {
        reset: jest.fn(),
        gridSize: 1,
        setGridSize: jest.fn()
    };

    it('Should Render properly', () => {
        const wrapper = shallow(<Controls {...defaultProps} />);
        expect(wrapper.hasClass('controls')).toBe(true);
    });
});