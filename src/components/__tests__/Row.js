import React from 'react';
import Row from '../Row';
import { shallow } from 'enzyme';
import Cell from '../Cell';

describe('Row component tests', () => {
    const defaultProps = {
        data: [true, false],
        rowId: 1,
        toggleCellState: jest.fn()
    };

    it('Should Render properly', () => {
        const wrapper = shallow(<Row {...defaultProps} />);
        expect(wrapper.hasClass('row')).toBe(true);
        expect(wrapper.find(Cell)).toHaveLength(2);
    });
});