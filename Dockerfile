# Stage 0, build the react app with node container
FROM node:10.19.0 AS development

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install --quiet

# Bundle app source
COPY . .

# Build the react app
RUN npm run build

ENV CI true
RUN npm test

FROM nginx:alpine AS production

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=development /usr/src/app/build /usr/share/nginx/html
